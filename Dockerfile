FROM debian:9 as build

RUN apt update && apt install -y nano wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure && make && make install

FROM debian:9
RUN apt update && apt install -y libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin .
COPY --from=build /usr/local/nginx/conf/* /usr/local/nginx/conf/
COPY --from=build /usr/local/nginx/html/* /usr/local/nginx/html/

RUN mkdir ../logs && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]	
